import 'package:exercise3/exercise3.dart' as exercise3;
import 'dart:io';

bool isPrime(number) {
  for (var i = 2; i <= number / i; i++) {
    if (number % i == 0) {
      return false;
    }
  }
  return true;
}

void main() {
  print('Enter number : ');
  var number = int.parse(stdin.readLineSync()!);
  if (isPrime(number)) {
    print('$number is a prime number.');
  } else {
    print('$number is not a prime number.');
  }
}
